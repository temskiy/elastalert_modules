Модуль для отправки уведомления в slack с использованием токена а не webhook.
За основу взят родной модуль SlackAlerter.

Для работы необходимо задать **slack_api_url** и **slack_token**. Остальные опции аналогичны опциям "родного" модуля slack.

```
alert: "elastalert_modules.slack_use_token.SlackUseTokenAlerter"
slack_api_url: "https://slack.com/api/chat.postMessage"
slack_token: "xoxb-***********************************"
```

Для установки склонировать репозиторий в одну из директорий в которых python производит поиск пакетов